import PropTypes from 'prop-types';

export default function UserInput({userInput, handleChange}) {
    return (
        <section id="user-input">
            <div className="input-group">
                <p>
                    <label>Initial Investment</label>
                    <input type="number" required
                           value={userInput.initialInvestment}
                           onChange={(event) => handleChange('initialInvestment', event.target.value)}/>
                </p>
                <p>
                    <label>Annual Investment</label>
                    <input type="number" value={userInput.annualInvestment} required onChange={(event) => handleChange('annualInvestment', event.target.value)}/>
                </p>
            </div>
            <div className="input-group">
                <p>
                    <label>Expected Return</label>
                    <input type="number" value={userInput.expectedReturn} required onChange={(event) => handleChange('expectedReturn', event.target.value)}/>
                </p>
                <p>
                    <label>Duration</label>
                    <input type="number" value={userInput.duration} required onChange={(event) => handleChange('duration', event.target.value)}/>
                </p>
            </div>
        </section>
    )
}

UserInput.propTypes = {
    userInput: PropTypes.shape({
        initialInvestment: PropTypes.number,
        annualInvestment: PropTypes.number,
        expectedReturn: PropTypes.number,
        duration: PropTypes.number
    }),
    handleChange: PropTypes.func
}
