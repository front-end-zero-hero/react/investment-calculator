import logo from "../assets/investment-calculator-logo.png";

const Header = () => {
    return (
        <div id="header">
            <img src={logo} alt="investment calculator"/>
        </div>
    )
}

export default Header;
